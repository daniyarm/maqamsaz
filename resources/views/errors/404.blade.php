@extends('layouts.main')
@section('content')
<section class="section-404">
    <div class="container">
        <img src="img/main/404.png" alt="">
        <h1>Бет табылмады</h1>
        <p>Парақ ескірген, жойылған немесе мүлде болған жоқ</p>
        <a href="/" class="btn-plain btn-orange">Басты бетке оралу</a>
    </div>
</section>
@endsection