@extends('layouts.main')
@section('content')
    <section class="main">
        <div class="main-bg">
            <img src="{{$sliders[0]["slider_image"]}}" alt="">
        </div>
        <div class="container">
            <div class="main-caption">
                {{--<h1>{{$sliders[0]["header"]}}</h1>--}}
                <img src="/img/main/404.png" alt="">
            </div>
        </div>
    </section>
    <section class="type bg-grey">
        <div class="container">
            <div class="type-cover">
                <h2>Бөлімдер</h2>
                <div class="row">
                    @foreach($rubrics as $item)
                        <div class="col-md-4 col-sm-4">
                            <a href="/category/{{$item->rubric_id}}">
                                <div class="type-item">
                                    <div class="type-item-img">
                                        <img src="{{$item->photo}}" alt="">
                                    </div>
                                    <div class="type-item-caption">
                                        <h3>
                                            {{$item->rubric_name}}
                                        </h3>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="description-base">
                <div class="row">
                    <div class="col-md-3 col-sm-4">
                        <div class="description-base-img">
                            <img src="img/main/logo-lg.png" alt="">
                        </div>
                    </div>
                    <div class="col-md-9 col-sm-8">
                        <div class="description-base-caption">
                            <h2>{{$sliders[1]["header"]}}</h2>
                            <p>
                                {{$sliders[1]["slider_text"]}}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection