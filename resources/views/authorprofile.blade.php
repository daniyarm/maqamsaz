@extends('layouts.main')
@section('content')
    <section class="profile">

        <div class="container">
            <ol class="breadcrumb">
                <li><a href="/">Басты бет</a></li>
                <li class="active">{{$author->author_fio}}</li>
            </ol>
            <div class="row">
                <div class="col-md-3 col-sm-4">
                    <div class="profile-img">
                        <img src="{{$author->author_photo}}" alt="">
                    </div>
                </div>
                <div class="col-md-9 col-sm-8">
                    <div class="profile-caption">
                        <h1>{{$author->author_fio}}</h1>
                        {!! $author->author_description !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="category">
        <div class="container">
            <h2 class="subtitle">Шығармалар:</h2>
            <div class="row">
                @foreach($shigarmalar as $item)
                <div class="col-md-2 col-sm-3">
                    <div class="category-item">
                        <div class="category-item-img">
                            <img src="{{$item->photo}}" alt="">
                            <div class="overlay-play">
                                <div class="play-info">
                                    <img src="img/icon/play.png" alt="">
                                    <span>{{$item->duration}}</span>
                                </div>
                                <a href="/shigarma/{{$item->id}}" class="btn-plain play-lg">
                                    <img src="img/icon/play-lg.png" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="category-item-caption">
                            <h3>{{$item->name}}</h3>
                            @foreach($item->authors as $item)
                                <p>{{$item->author_fio}}</p>
                            @endforeach
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            {{$shigarmalar->links()}}
        </div>
    </section>
@endsection