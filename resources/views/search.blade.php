@extends('layouts.main')
@section('content')
<section class="category search-box">
    <div class="container">
        <h1><span>Iздеу нәтижесі:</span> {{$search}}</h1>

        <div class="row">
            @foreach($shigarmalar as $item)
                <div class="col-md-2 col-sm-3">
                    <div class="category-item">
                        <div class="category-item-img">
                            <img src="{{$item->photo}}" alt="">
                            <div class="overlay-play">
                                <div class="play-info">
                                    <img src="/img/icon/play.png" alt="">
                                    <span>05:31</span>
                                </div>
                                <a href="/shigarma/{{$item->id}}" class="btn-plain play-lg">
                                    <img src="/img/icon/play-lg.png" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="category-item-caption">
                            <h3>{{$item->name}}</h3>
                            @foreach($item->authors as $item)
                                <p>{{$item->author_fio}}</p>
                            @endforeach
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        {{ $shigarmalar->links() }}
    </div>
</section>
@endsection