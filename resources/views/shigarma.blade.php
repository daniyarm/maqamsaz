@extends('layouts.main')
@section('style')
    <link rel="stylesheet" href="/css/audioplayer.css">
    <link rel="stylesheet" href="/css/lg-transitions.min.css">
    <link rel="stylesheet" href="/css/lightgallery.min.css">
@endsection
@section('content')
    <section class="category">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="/">Басты бет</a></li>
                <li><a href="/category/{{$shigarma->rubric["rubric_id"]}}">{{$shigarma->rubric["rubric_name"]}}</a></li>
                <li class="active">{{$shigarma->name}}</li>
            </ol>
            <div class="row">
                <div class="col-md-3 col-sm-4">
                    <div class="inside-img">
                        <img src="{{$shigarma->photo}}" alt="">
                    </div>
                    @if($shigarma->notas)
                    <div class="inside-btn d-flex-justify">
                        <img src="/img/icon/note.png">
                        <p>Нота</p>
                        <button class="btn-plain dynamic"><a target="blank" href="{{($shigarma->notas)?$shigarma->notas["path"]:'#'}}"><img src="/img/icon/arr-white.png" alt=""></a></button>
                    </div>
                    @endif
                </div>
                <div class="col-md-9 col-sm-8">
                    <div class="inside-caption">
                        <h2>{{$shigarma->name}}</h2>
                        {!! $shigarma->description !!}
                        <div class="inside-info">
                            <p>
                                <span>Автор:</span>
                                @foreach($shigarma->authors as $key=>$item)
                                    @if ($key == count($shigarma->authors) - 1)
                                        <a href="/author/{{$item->author_id}}">{{$item->author_fio}}</a>
                                    @else
                                        <a href="/author/{{$item->author_id}}">{{$item->author_fio}}</a> ,
                                    @endif
                                @endforeach
                            </p>
                            <p>
                                <span>Орындаушы:</span>
                                @foreach($shigarma->orindauwilar as $key=>$item)
                                    @if ($key == count($shigarma->orindauwilar) - 1)
                                        <a href="/orindaushi/{{$item->id}}">{{$item->name}}</a>
                                    @else
                                        <a href="/orindaushi/{{$item->id}}">{{$item->name}}</a> ,
                                    @endif
                                @endforeach
                            </p>
                        </div>
                    </div>
                    <div class="audio-box">
                        @foreach($shigarma->orindauwilar as $key=>$item)
                            <div class="audio-cover">
                                <audio preload="auto" controls>
                                    <source src="{{$item->pivot["path"]}}">
                                </audio>

                                <!--<audio src="audio.wav" preload="auto" controls autoplay loop></audio>-->
                                <!--<br type="_moz">-->
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script src="/js/audioplayer.js"></script>
    <script src="/js/lightgallery.min.js"></script>
    <script src="/js/lightgallery-all.min.js"></script>
    <script type="text/javascript">
        /*$('.dynamic').on('click', function() {

            $(this).lightGallery({
                dynamic: true,
                dynamicEl: [{
                    "src": '/img/main/bobek.png',
                    'thumb': '/img/main/bobek.png',
                    'subHtml': '<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>'
                }, {
                    'src': '/img/main/bobek.png',
                    'thumb': '/img/main/bobek.png',
                    'subHtml': "<h4>Bowness Bay</h4><p>A beautiful Sunrise this morning taken En-route to Keswick not one as planned but I'm extremely happy I was passing the right place at the right time....</p>"
                }, {
                    'src': '/img/main/bobek.png',
                    'thumb': '/img/main/bobek.png',
                    'subHtml': "<h4>Coniston Calmness</h4><p>Beautiful morning</p>"
                }]
            })

        });*/
        //    $('.note').lightGallery();
        $(function() {
            $('audio').audioPlayer();
        });
    </script>
@endsection