@extends('admin.layouts.layout')

@section('css')
@endsection

@section('content')
    <div class="page-wrapper" style="min-height: 319px;">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-8 col-8 align-self-center">
                    <h3 class="text-themecolor m-b-0 m-t-0">
                        Добавить слайд
                    </h3>
                </div>
                <div class="col-md-4 col-4 align-self-center text-right">
                    <a href="/admin/slider" class="btn btn-danger">Назад</a>
                </div>
            </div>
            <div class="row">
                @if(empty($slider))
                    <form class="col-lg-12 col-md-12 row" action="/admin/slider" method="POST" enctype="multipart/form-data">
                        @else
                            <form class="col-lg-12 col-md-12 row" action="/admin/slider/{{ $slider->slider_id }}" method="POST" enctype="multipart/form-data">
                                @method('PUT')
                                @endif
                                @csrf
                                <div class="col-lg-8 col-md-12">
                                    <div class="card">
                                        <div class="card-block">
                                            @if ($errors->any())
                                                <div class="alert alert-danger">
                                                    <ul>
                                                        @foreach ($errors->all() as $error)
                                                            <li>{{ $error }}</li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @endif
                                            <div class="box-body">
                                                <div class="form-group">
                                                    <label>Заголовок</label>
                                                    <input name="header" class="form-control" value="{{ !empty($slider) ? $slider->header : old('header') }}">
                                                </div>
                                                <div class="form-group">
                                                    <label>Текст</label>
                                                    <textarea name="slider_text" class="form-control">{{ !empty($slider) ? $slider->slider_text : old('slider_text') }}</textarea>
                                                </div>
                                                <div class="form-group">
                                                    <label>Ссылка</label>
                                                    <input type="text" class="form-control" name="slider_url" value="{{ !empty($slider) ? $slider->slider_url : old('slider_url') }}" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-12">
                                    <div class="card">
                                        <div class="card-block">
                                            <div class="box box-primary" style="padding: 30px; text-align: center">

                                                    <div style="padding: 20px; border: 1px solid #c2e2f0">
                                                        <img class="image-src" id="blah3" src="{{ !empty($slider) ? $slider->slider_image : old('slider_image') }}"
                                                             style="width: 100%; " />
                                                    </div>
                                                    <div style="background-color: #c2e2f0;height: 40px;margin: 0 auto;width: 2px;">
                                                    </div>
                                                    <label class="btn btn-primary label-img" for="imgInp3">
                                                        <input id="imgInp3" type="file" name="slider_image" class="d-none">
                                                        <i class="fa fa-plus"></i>
                                                    </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-8 col-md-12 text-right">
                                    <div class="box-footer">
                                        <button type="submit" class="btn btn-primary">Сохранить</button>
                                    </div>
                                </div>
                            </form>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                let reader = new FileReader();
                reader.onload = function (e) {
                    inpid = $(input).attr('id');
                    inpid = inpid[inpid.length - 1];
                    imgId = '#blah' + inpid;
                    $(imgId).attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#imgInp3").change(function () {
            readURL(this);
        });
    </script>
@endsection