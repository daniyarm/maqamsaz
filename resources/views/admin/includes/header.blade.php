<header>
    <div class="container">
        <nav class="navbar d-flex-justify">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/">
                    <img src="/img/logo/logo.png" alt="">
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="/">Басты бет</a></li>
                    @foreach($rubrics as $item)
                        <li><a href="/category/{{$item->rubric_id}}">{{$item->rubric_name}}</a></li>
                    @endforeach
                </ul>
                <form action="/search" method="post">
                    @csrf
                    <label class="label-search d-flex">
                        <button class="btn-plain">
                            <img src="/img/icon/search.svg" alt="">
                        </button>
                        <input type="text" name="search" placeholder="Сайт бойынша іздеу">
                    </label>
                </form>
            </div><!-- /.navbar-collapse -->
        </nav>
    </div>
</header>