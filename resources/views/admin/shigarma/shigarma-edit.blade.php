@extends('admin.layouts.layout')

@section('css')
    <link rel="stylesheet" href="/css/image-uploader.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
@endsection

@section('content')
    <div class="page-wrapper" style="min-height: 319px;">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-8 col-8 align-self-center">
                    <h3 class="text-themecolor m-b-0 m-t-0">
                        Добавить шығарма
                    </h3>
                </div>
                <div class="col-md-4 col-4 align-self-center text-right">
                    <a href="/admin/shigarma" class="btn btn-danger">Назад</a>
                </div>
            </div>
            <div class="row">

                            <form class="col-lg-12 col-md-12 row" action="/admin/shigarma/{{ $shigarma->id }}" method="POST" enctype="multipart/form-data">
                                @method('PUT')

                                @csrf
                                <div class="col-lg-8 col-md-12">
                                    <div class="card">
                                        <div class="card-block">
                                            @if ($errors->any())
                                                <div class="alert alert-danger">
                                                    <ul>
                                                        @foreach ($errors->all() as $error)
                                                            <li>{{ $error }}</li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @endif
                                            <div class="box-body">
                                                <div class="form-group">
                                                    <label>Название</label>
                                                    <input type="text" class="form-control" name="name" value="{{$shigarma->name}}" />
                                                </div>
                                                <div class="form-group">
                                                    <label>Описание</label>
                                                    <textarea id="editor" name="description">{{$shigarma->description}}</textarea>
                                                </div>
                                                <div class="form-group">
                                                    <label>Рубрика</label>
                                                    <select class="form-control" name="rubric_id">
                                                        @foreach($rubrics as $item)
                                                            <option {{($item->rubric_id == $shigarma->rubric_id)?'selected':''}} value="{{$item->rubric_id}}">{{$item->rubric_name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                                @if($shigarma->notas)
                                                    <a download href="{{$shigarma->notas["path"]}}">Скачать</a>
                                                @endif
                                                <br>
                                                <div class="form-group">
                                                    <label>Нота</label>
                                                    <input type="file" name="images" class="form-control">
                                                </div>

                                                <div class="form-group">
                                                <input name="authors" placeholder="Авторларды тандаңыз"
                                                      value="
                                                                        @foreach($shigarma->authors as $author)
                                                                            {{$author->author_fio}},
                                                                        @endforeach
                                                               ">
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-12">
                                    <div class="card">
                                        <div class="card-block">
                                            <div class="box box-primary" style="padding: 30px; text-align: center">
                                                    <div class="tab-pane fade show active" id="img-ru" role="tabpanel"
                                                         aria-labelledby="img-ru-tab">
                                                        <div style="padding: 20px; border: 1px solid #c2e2f0">
                                                            <img class="image-src" id="blah1" src="{{ $shigarma->photo}}"
                                                                 style="width: 100%; " />
                                                        </div>
                                                        <div style="background-color: #c2e2f0;height: 40px;margin: 0 auto;width: 2px;">
                                                        </div>
                                                        <label class="btn btn-primary label-img" for="imgInp1">
                                                            <input id="imgInp1" type="file" name="photo" class="d-none">
                                                            <i class="fa fa-plus"></i>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-8 col-md-12 text-right">
                                    <div class="box-footer">
                                        <button type="submit" class="btn btn-primary">Сохранить</button>
                                    </div>
                                </div>
                            </form>
            </div>
        </div>
    </div>
@endsection
@section('js')

    <script type="text/javascript" src="/js/image-uploader.min.js"></script>



    <script>
        var array=[];
        @foreach($authors as $item)
        array.push("{{$item->author_fio}}");
        @endforeach
        $(document).ready(function() {
            $('[name=authors]').tagify({
                delimiters: ",",
                pattern: null,
                maxTags: Infinity,
                callbacks: {},
                addTagOnBlur: true,
                duplicates: false,
                whitelist: array,
                blacklist: [],
                enforceWhitelist: true,
                autoComplete: true
            });
        });
    </script>
    <script>

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    inpid = $(input).attr('id');
                    inpid = inpid[inpid.length - 1];
                    imgId = '#blah' + inpid;
                    $(imgId).attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imgInp1").change(function () {
            readURL(this);
        });
        $("#imgInp2").change(function () {
            readURL(this);
        });
        $("#imgInp3").change(function () {
            readURL(this);
        });
    </script>
@endsection