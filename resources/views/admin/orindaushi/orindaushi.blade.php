@extends('admin.layouts.layout')

@section('css')

@endsection

@section('content')
<div class="page-wrapper" style="min-height: 319px;">
  <div class="container-fluid">
    <div class="row page-titles">
      <div class="col-md-8 col-8 align-self-center">
        <h3 class="text-themecolor m-b-0 m-t-0 d-inline-block">
          <a>Орындаушы</a>
        </h3>
      </div>
      <div class="col-md-4 col-4 align-self-center text-right">
        <a href="/admin/orindaushi/create" class="btn btn-success">Добавить</a>
      </div>
    </div>

    <div class="row white-bg">
      <div class="col-md-12">
        <div class="box-body">
          <div class="table-responsive">
            <table id="showed" class="table table-bordered table-striped">
              <thead>
                <tr style="border: 1px">
                  <th style="width: 30px">№</th>
                  <th>ФИО</th>
                  <th>фото</th>
                  <th></th>
                    <th></th>
                </tr>
              </thead>

              <tbody>
                @foreach ($orindaushi as $value)
                <tr>
                  <td>{{ $value->id }}</td>
                  <td>{{ $value->name }}</td>
                  <td><img style="width:100px;" src="{{ $value->photo }}"></td>
                  <td>
                    <a href="javascript:void(0)" onclick="remove(this,'{{ $value->id }}','orindaushi')">
                      <i class="fas fa-trash"></i>
                    </a>
                  </td>
                    <td><a href="/admin/orindaushi/{{ $value->id }}/edit"><i class="fas fa-pen"></i></a></td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>


        </div>
      </div>
    </div>
  </div>
</div>

@endsection

@section('js')
<script>
  $('#send').click(function () {
    $.post("/admin/orindaushi", {
      name: $('input[name=orindaushi_name]').val(),
      _token: '{{csrf_token()}}'
    }, function (response) {
      $('tbody').append(
        "<tr><td>" + response.id + "</td>" +
        "<td>" + response.name + "</td>" +
        "<td><a href=\"javascript:void(0)\" onclick=\"remove(this,'" + response.id + "','orindaushi')\"><i class=\"fas fa-trash\"></i></a></td>" +
          " <td><a href=\"/admin/orindaushi/" + response.id +"/edit\"><i class=\"fas fa-pen\"></i></a></td></tr>"

      );
    });
  });
</script>
@endsection