@extends('admin.layouts.layout')

@section('css')

@endsection

@section('content')
  <div class="page-wrapper" style="min-height: 319px;">
    <div class="container-fluid">
      <div class="row page-titles">
        <div class="col-md-8 col-8 align-self-center">
          <h3 class="text-themecolor m-b-0 m-t-0 d-inline-block">
            <a>Рубрика</a>
          </h3>
        </div>
        <div class="col-md-4 col-4 align-self-center text-right">
          <a href="/admin/rubric/create" class="btn btn-success">Добавить</a>
        </div>
      </div>

      <div class="row white-bg">
        <div class="table-responsive">
          <table id="showed" class="table table-bordered table-striped">
            <thead>
            <tr style="border: 1px">
              <th style="width: 30px">№</th>
              <th>Название</th>
              <th>фото</th>
              <th></th>
              <th></th>
            </tr>
            </thead>

            <tbody>
            @foreach ($rubric as $value)
              <tr>
                <td>{{ $value->rubric_id }}</td>
                <td>{{ $value->rubric_name }}</td>
                <td><img style="width:150px;" src="{{ $value->photo }}" ></td>
                <td>
                  <a href="javascript:void(0)" onclick="remove(this,'{{ $value->rubric_id }}','rubric')">
                    <i class="fas fa-trash"></i>
                  </a>
                </td>
                <td><a href="/admin/rubric/{{ $value->rubric_id }}/edit"><i class="fas fa-pen"></i></a></td>
              </tr>
            @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

@endsection

@section('js')
  <script>
      $('#send').click(function () {
          $.post("/admin/rubric", {
              rubric_name: $('input[name=rubric_name]').val(),
              photo: $('input[name=photo]').val(),
              _token: '{{csrf_token()}}'
          }, function (response) {
              $('tbody').append(
                  "<tr><td>" + response.rubric_id + "</td>" +
                  "<td>" + response.rubric_name + "</td>" +
                  "<td><a href=\"javascript:void(0)\" onclick=\"remove(this,'" + response.rubric_id + "','rubric')\"><i class=\"fas fa-trash\"></i></a></td>" +
                  "<td><a href=\"/admin/rubric/" + response.rubric_id +"/edit\"><i class=\"fas fa-pen\"></i></a></td></tr>"
              );
          });
      });
  </script>
  <script>
      function readURL(input) {
          if (input.files && input.files[0]) {
              var reader = new FileReader();
              reader.onload = function (e) {
                  inpid = $(input).attr('id');
                  inpid = inpid[inpid.length - 1];
                  imgId = '#blah' + inpid;
                  $(imgId).attr('src', e.target.result);
              }
              reader.readAsDataURL(input.files[0]);
          }
      }

      $("#imgInp1").change(function () {
          readURL(this);
      });
      $("#imgInp2").change(function () {
          readURL(this);
      });
      $("#imgInp3").change(function () {
          readURL(this);
      });
  </script>
@endsection