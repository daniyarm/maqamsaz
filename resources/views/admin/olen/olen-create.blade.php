@extends('admin.layouts.layout')
@section('css')
@endsection
@section('content')
    <div class="page-wrapper" style="min-height: 319px;">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-8 col-8 align-self-center">
                    <h3 class="text-themecolor m-b-0 m-t-0">
                        Добавить олен
                    </h3>
                </div>
                <div class="col-md-4 col-4 align-self-center text-right">
                    <a href="/admin/olen" class="btn btn-danger">Назад</a>
                </div>
            </div>
            <div class="row">
                <form class="col-lg-12 col-md-12 row" action="/admin/olen" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="col-lg-12 col-md-12">
                        <div class="card">
                            <div class="card-block">
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <div class="box-body">
                                    <div class="form-group">
                                        <label>Шагырма</label>
                                        <select class="form-control" name="shigarma_id">
                                            @foreach($shigarmalar as $item)
                                                <option value="{{$item->id}}">{{$item->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Орындаушы</label>
                                        <select class="form-control" name="orindaushi_id">
                                            @foreach($orindaushi as $item)
                                                <option value="{{$item->id}}">{{$item->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group files" id="files1">
                                    <span class="btn btn-success btn-file">
                                        Добавить файл
                                        <input type="file" name="archive_file" />
                                    </span>
                                        <br />
                                        <ul class="fileList m-t-20">
                                            @if(!empty($archive))
                                                @php
                                                    $pice = str_replace("'", "", substr($archive->archive_file, 0, -1));
                                                    $links = explode(",", $pice);
                                                @endphp
                                                @foreach ($links as $item)
                                                    <li>
                                                        <strong>{{ $item }}</strong>&nbsp; &nbsp;
                                                        <a class="removeFile text-danger" href="#" data-fileid="0">✕</a>
                                                    </li>
                                                @endforeach
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-12 text-right">
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Сохранить</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="/js/fileupload.js"></script>
    <script>
        $("#files1").fileUploader(filesToUpload);
    </script>
@endsection