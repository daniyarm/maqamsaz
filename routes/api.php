<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['namespace' => 'API'], function() {

    Route::group(['middleware' => 'api'], function() {
        Route::get('mainpage', 'MainController@index');
        Route::get('category', 'CategoryController@index');
        Route::get('shigarma', 'ShigarmaController@index');
        Route::get('player', 'ShigarmaController@getsongs');
        Route::get('nota', 'ShigarmaController@getnota');
        Route::get('author', 'AuthorController@authorprofile');
        Route::get('orindaushi', 'AuthorController@getorindaushiprofile');
        Route::get('searchapi', 'ShigarmaController@search');
        Route::get('searchincatalog', 'ShigarmaController@searchincatalog');
        Route::get('catalog', 'MainController@catalog');
    });
});





