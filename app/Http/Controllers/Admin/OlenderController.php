<?php

namespace App\Http\Controllers\Admin;

use App\Models\Orindaushi;
use App\Models\Shigarma;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Author;
use App\Models\OrindaushiOleni;
use App\Http\Helpers;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use wapmorgan\Mp3Info\Mp3Info;

class OlenderController extends Controller
{
    public function index()
    {
        $olender = OrindaushiOleni::all();
        return view('admin.olen.olen', compact('olender'));
    }
    public function create()
    {
        $shigarmalar = Shigarma::all();
        $orindaushi = Orindaushi::all();
        return view('admin.olen.olen-create', compact('shigarmalar','orindaushi'));
    }
    public function store(Request $request)
    {
        $request->validate([
            'orindaushi_id' => 'required',
            'shigarma_id'=>'required',

        ]);

        if ($request->hasFile('archive_file')) {
            $cover = $request->file('archive_file');
            $resultall = "";

                $file_name = $cover->getClientOriginalName();
                $extension = $cover->getClientOriginalExtension();

                $destinationPath = $request->disk . '/' . date('Y') . '/' . date('m') . '/' . date('d');

                $file_name = $destinationPath . '/' . $file_name;

                if (Storage::disk('doc')->exists($file_name)) {
                    $now = \DateTime::createFromFormat('U.u', microtime(true));
                    $file_name = $destinationPath . '/' . $now->format("Hisu") . '.' . $extension;
                }

                Storage::disk('doc')->put($file_name, File::get($cover));
                $resultall .= '/media_doc' . $file_name;

        }

        $getID3 = new \getID3;
        $file = $getID3->analyze($cover);


        OrindaushiOleni::create([
            'shigarma_id'=>$request->shigarma_id,
            'orindaushi_id'=>$request->orindaushi_id,
            'path'=>$resultall,
            'duration'=>$file['playtime_string']
        ]);

        Shigarma::where('id',$request->shigarma_id)->update(['duration'=>$file['playtime_string']]);



        return redirect('/admin/olen');
    }
    public function show($id)
    {
        //
    }
    public function edit($id)
    {
        $shigarmalar = Shigarma::all();
        $orindaushi = Orindaushi::all();
        $olen = OrindaushiOleni::where('id',$id)->first();
        return view('admin.olen.olen-edit', compact('olen','shigarmalar','orindaushi'));
    }
    public function update(Request $request, $id)
    {
        $request->validate([
            'orindaushi_id' => 'required',
            'shigarma_id'=>'required',

        ]);

        if ($request->hasFile('archive_file')) {
            $cover = $request->file('archive_file');
            $resultall = "";

            $file_name = $cover->getClientOriginalName();
            $extension = $cover->getClientOriginalExtension();

            $destinationPath = $request->disk . '/' . date('Y') . '/' . date('m') . '/' . date('d');

            $file_name = $destinationPath . '/' . $file_name;

            if (Storage::disk('doc')->exists($file_name)) {
                $now = \DateTime::createFromFormat('U.u', microtime(true));
                $file_name = $destinationPath . '/' . $now->format("Hisu") . '.' . $extension;
            }

            Storage::disk('doc')->put($file_name, File::get($cover));
            $resultall .= '/media_doc' . $file_name;

        }

        OrindaushiOleni::find($id)->update([
            'shigarma_id'=>$request->shigarma_id,
            'orindaushi_id'=>$request->orindaushi_id,
            'path'=>$resultall
        ]);

        return redirect('/admin/olen');
    }
    public function destroy($id)
    {
        $user = OrindaushiOleni::where('id',$id)->first();
        $user->delete(); 
    }
}
