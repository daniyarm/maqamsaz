<?php

namespace App\Http\Controllers\Admin;

use App\Models\Rubric;
use App\Models\Nota;
use App\Models\ShigarmaAuthor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Shigarma;
use App\Models\Author;
use App\Http\Helpers;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
class ShigarmalarController extends Controller
{
    public function index()
    {
        $shigarmalar = Shigarma::all();
        return view('admin.shigarma.shigarma', compact('shigarmalar'));
    }
    public function create()
    {
        $authors=Author::all();
        $rubrics=Rubric::all();
        return view('admin.shigarma.shigarma-create',compact('authors','rubrics'));
    }
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'rubric_id' => 'required',
            'authors' => 'required',
            'photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);


        if($request->has('authors')){
            $vowels = array("{", "}", "\"", ":", "[", "]");
            $onlyconsonants = str_replace($vowels, "", $request->authors);
            $tags =explode(",", $onlyconsonants);
            $tags2=[];
            foreach ($tags as $item){
                array_push($tags2,substr($item, 5));
            }
        }

        if ($request->hasFile('photo')) {
            $result = Helpers::storeImg('photo', 'avatar', $request);
        }else {
            $result = '/img/default-user.png';
        }


        $shigarma = new Shigarma();
        $shigarma->name = $request->name;
        $shigarma->description = $request->description;
        $shigarma->rubric_id = $request->rubric_id;
        $shigarma->photo = $result;
        $shigarma->save();
        if ($request->hasFile('images')) {
            $resultall = Helpers::storeImg('images', 'image', $request);
                Nota::create(['shigarma_id' => $shigarma->id, 'path' => $resultall]);

        }
        foreach($tags2 as $item){
            $author = Author::where('author_fio',$item)->first()->author_id;
            ShigarmaAuthor::create(['shigarma_id'=>$shigarma->id,'author_id'=>$author]);
        }


        return redirect('/admin/shigarma');
    }
    public function show($id)
    {
        //
    }
    public function edit($id)
    {
        $shigarma = Shigarma::where('id', $id)->first();
        $authors=Author::all();
        $rubrics= Rubric::all();

        return view('admin.shigarma.shigarma-edit', compact('shigarma','rubrics','authors'));
    }
    public function update(Request $request, $id)
    {

        if($request->has('authors')){
            $vowels = array("{", "}", "\"", ":", "[", "]");
            $onlyconsonants = str_replace($vowels, "", $request->authors);
            $tags =explode(",", $onlyconsonants);
            $tags2=[];
            foreach ($tags as $item){
                array_push($tags2,substr($item, 5));
            }
        }


        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'rubric_id' => 'required',
            'authors' => 'required',
            'photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);



        if ($request->hasFile('photo')) {
            $result = Helpers::storeImg('photo', 'avatar', $request);
        }else {
            $result = '/img/default-user.png';
        }


        $shigarma =  Shigarma::where('id',$id)->first();
        $shigarma->name = $request->name;
        $shigarma->description = $request->description;
        $shigarma->rubric_id = $request->rubric_id;
        $shigarma->photo = $result;
        $shigarma->save();

        if ($request->hasFile('images')) {
            $resultall = Helpers::storeImg('images', 'image', $request);
            $nota = Nota::where("shigarma_id",$shigarma->id)->first();
            if($nota){
                $nota->update(['shigarma_id' => $shigarma->id, 'path' => $resultall]);
            }
            else{
                $resultall = Helpers::storeImg('images', 'image', $request);
                Nota::create(['shigarma_id' => $shigarma->id, 'path' => $resultall]);
            }

        }

        ShigarmaAuthor::where('shigarma_id',$shigarma->id)->delete();

        foreach($tags2 as $item){
            $author = Author::where('author_fio',$item)->first()->author_id;
            ShigarmaAuthor::create(['shigarma_id'=>$shigarma->id,'author_id'=>$author]);
        }


        return redirect('/admin/shigarma');
    }
    public function destroy($id)
    {
        $user = Shigarma::where('id',$id)->first();
        $user->delete();
    }
    public function search(Request $request){
        $shigarmalar = Shigarma::where('name','like' , '%'.$request->search.'%')->paginate(12);

        $authors = Author::where('author_fio','like' , '%'.$request->search.'%')->paginate(12);


        $search=$request->search;

        return view('search' , compact('shigarmalar','search'));
    }
}
