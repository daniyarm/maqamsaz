<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Author;
use App\Http\Helpers;

class AuthorController extends Controller
{
    public function index()
    {
        $users = Author::all();
        return view('admin.author.author', compact('users'));
    }
    public function create()
    {
        return view('admin.author.author-create');
    }
    public function store(Request $request)
    {
        $request->validate([
            'author_fio' => 'required',
            'author_description' => 'required',
            'author_photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]); 

        if ($request->hasFile('author_photo')) {
            $result = Helpers::storeImg('author_photo', 'avatar', $request);
        }else {
            $result = '/img/default-user.jpg';
        }

        $users = new Author();
        $users->author_fio = $request->author_fio;
        $users->author_description = $request->author_description;
        $users->author_photo = $result;
        $users->save();

        return redirect('/admin/authors');
    }
    public function show($id)
    {
        //
    }
    public function edit($id)
    {
        $author = Author::where('author_id', $id)->first();

        return view('admin.author.author-edit', compact('author'));
    }
    public function update(Request $request, $id)
    {
        $request->validate([
            'author_fio' => 'required',
            'author_description' => 'required'
        ]);
        $users = Author::where('author_id',$id)->first();


        if ($request->hasFile('author_photo')) {
            $result = Helpers::storeImg('author_photo', 'avatar', $request);
            $users->author_photo = $result;
        }

        $users->author_fio = $request->author_fio;
        $users->author_description = $request->author_description;

        $users->save();

        return redirect('/admin/authors');
    }
    public function destroy($id)
    {
        $user = Author::find($id);
        $user->delete(); 
    }
}
