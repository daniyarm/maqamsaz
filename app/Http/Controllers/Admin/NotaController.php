<?php

namespace App\Http\Controllers\Admin;

use App\Models\Orindaushi;
use App\Models\Shigarma;
use App\Models\Nota;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Author;
use App\Models\OrindaushiOleni;
use App\Http\Helpers;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class NotaController extends Controller
{
    public function index()
    {
        $olender = Nota::all();
        return view('admin.nota.nota', compact('olender'));
    }
    public function create()
    {
        $shigarmalar = Shigarma::all();
        return view('admin.nota.nota-create', compact('shigarmalar'));
    }
    public function store(Request $request)
    {
        $request->validate([
            'shigarma_id'=>'required'
        ]);


        if ($request->hasFile('nota')) {
            $images = $request->nota;
            foreach ($images as $item) {
                $result2 = Helpers::storeArrayImg($item, 'image', $request);

                Nota::create([
                    'shigarma_id'=>$request->shigarma_id,
                    'path'=>$result2
                ]);
            }
        }
        return redirect('/admin/nota');
    }
    public function show($id)
    {
        //
    }
    public function edit($id)
    {
        $shigarmalar = Shigarma::all();
        $orindaushi = Orindaushi::all();
        $olen = OrindaushiOleni::where('id',$id)->first();
        return view('admin.olen.olen-edit', compact('olen','shigarmalar','orindaushi'));
    }
    public function update(Request $request, $id)
    {
        $request->validate([
            'orindaushi_id' => 'required',
            'shigarma_id'=>'required',

        ]);

        if ($request->hasFile('archive_file')) {
            $cover = $request->file('archive_file');
            $resultall = "";

            $file_name = $cover->getClientOriginalName();
            $extension = $cover->getClientOriginalExtension();

            $destinationPath = $request->disk . '/' . date('Y') . '/' . date('m') . '/' . date('d');

            $file_name = $destinationPath . '/' . $file_name;

            if (Storage::disk('doc')->exists($file_name)) {
                $now = \DateTime::createFromFormat('U.u', microtime(true));
                $file_name = $destinationPath . '/' . $now->format("Hisu") . '.' . $extension;
            }

            Storage::disk('doc')->put($file_name, File::get($cover));
            $resultall .= '/media_doc' . $file_name;

        }

        OrindaushiOleni::find($id)->update([
            'shigarma_id'=>$request->shigarma_id,
            'orindaushi_id'=>$request->orindaushi_id,
            'path'=>$resultall
        ]);

        return redirect('/admin/olen');
    }
    public function destroy($id)
    {
        $user = OrindaushiOleni::where('id',$id)->first();
        $user->delete();
    }
}
