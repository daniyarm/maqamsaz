<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Rubric;
use App\Http\Helpers;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class RubricController extends Controller
{

    public function index()
    {
        $rubric = Rubric::all();
        return view('admin.rubric.rubric', compact('rubric'));
    }
    public function create()
    {
        return view('admin.rubric.rubric_create');
    }
    public function store(Request $request)
    {

        $rubric = new Rubric();
        $rubric->rubric_name	= $request->name;
        if ($request->hasFile('photo')) {
            $result = Helpers::storeImg('photo', 'image', $request);
            $rubric->photo	= $result;
        }
        $rubric->save();
        return redirect('/admin/rubric');
    }
    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $rubric = Rubric::where('rubric_id', $id)->first();
        return view('admin.rubric.rubric-edit', compact('rubric'));
    }
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required'
        ]);
        $users = Rubric::where('rubric_id',$id)->first();
        $users->rubric_name = $request->name;
        if ($request->hasFile('photo')) {
            $result = Helpers::storeImg('photo', 'image', $request);
            $users->photo	= $result;
        }

        $users->save();

        return redirect('/admin/rubric');
    }
    public function destroy($id)
    {
        $rubric = Rubric::find($id);
        $rubric->delete(); 
    }
    
}
