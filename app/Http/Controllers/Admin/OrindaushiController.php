<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Author;
use App\Models\Orindaushi;
use App\Http\Helpers;

class OrindaushiController extends Controller
{
    public function index()
    {
        $orindaushi = Orindaushi::all();
        return view('admin.orindaushi.orindaushi', compact('orindaushi'));
    }
    public function create()
    {
        return view('admin.orindaushi.create');
    }
    public function store(Request $request)
    {

        $request->validate([
            'name' => 'required',
             'description' => 'required',
        ],['required'=>"барлық мәліметтерді толтырыңыз"]);
        $orindaushi = new Orindaushi();
        if ($request->hasFile('photo')) {
            $result = Helpers::storeImg('photo', 'avatar', $request);
            $orindaushi->photo = $result;
        }
        $orindaushi->name = $request->name;
        $orindaushi->description = $request->description;
        $orindaushi->save();
        return redirect('/admin/orindaushi');
    }
    public function show($id)
    {
        //
    }
    public function edit($id)
    {
        $orindaushi = Orindaushi::where('id', $id)->first();

        return view('admin.orindaushi.orindaushi-edit', compact('orindaushi'));
    }
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required'
        ]);
        $users = Orindaushi::where('id',$id)->first();

        if ($request->hasFile('photo')) {
            $result = Helpers::storeImg('photo', 'avatar', $request);
            $users->photo = $result;

        }
        $users->name = $request->name;
        $users->description = $request->description;
        $users->save();
        return redirect('/admin/orindaushi');
    }
    public function destroy($id)
    {
        $user = Orindaushi::find($id);
        $user->delete(); 
    }
}
