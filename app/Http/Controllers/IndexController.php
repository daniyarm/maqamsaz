<?php

namespace App\Http\Controllers;

use App\Models\Rubric;
use App\Models\Slider;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $rubrics = Rubric::all();
        $sliders = Slider::all();
        return view('index' , compact('rubrics','sliders'));
    }
}
