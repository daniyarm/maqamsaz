<?php

namespace App\Http\Controllers\API;

use App\Models\Rubric;
use App\Models\Slider;
use App\Models\Shigarma;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MainController extends Controller
{
    public function __construct()
    {

    }
    public function index()
    {
        $sliders= Slider::select('header','slider_text' , 'slider_image')->get();
        $rubrics= Rubric::select('rubric_id','rubric_name' , 'photo')->get();
        $data['rubrics']=$rubrics;
        $data['sliders']=$sliders;

           if(count($rubrics)>0){
               return ['success' => true , 'data' =>$data];
           }
           else{
               return ['success' => true , 'data' =>'there is no rubric'];
           }

    }
    public function catalog(){
        $rubrics= Rubric::select('rubric_id','rubric_name')->get();
        if(count($rubrics)>0){
            return ['success' => true , 'data' =>$rubrics];
        }
        else{
            return ['success' => true , 'data' =>'there is no rubric'];
        }
    }
}
