<?php

namespace App\Http\Controllers\API;


use App\Models\Shigarma;
use App\Models\Nota;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Shigarma as ShigarmaResource;
use App\Http\Resources\Songs;
use App\Http\Resources\Category;

class ShigarmaController extends Controller
{

    public function index(Request $request)
    {
        $shigarma = Shigarma::where('id',$request->shigarma_id)->first();

        return new ShigarmaResource($shigarma);

    }
    public function getsongs(Request $request){
        $shigarma = Shigarma::where('id',$request->shigarma_id)->first();

        return new Songs($shigarma);
        }
    public function getnota(Request $request){
        $nota = Nota::where('shigarma_id',$request->shigarma_id)->select('path')->first();

        return ['success'=>true ,'nota'=>$nota];
    }
    public function search(Request $request){
        $shigarmalar = Shigarma::where('name','like' , '%'.$request->search.'%')->paginate(12);
        

        return new Category($shigarmalar);
    }
    public function searchincatalog(Request $request){
        $shigarmalar = Shigarma::where('rubric_id',$request->rubric_id)
            ->where('name','like' , '%'.$request->search.'%')
            ->paginate(12);
        return new Category($shigarmalar);
    }

}
