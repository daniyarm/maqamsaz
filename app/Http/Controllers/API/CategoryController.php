<?php

namespace App\Http\Controllers\API;


use App\Models\Shigarma;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Category;

class CategoryController extends Controller
{

    public function index(Request $request)
    {
        $category = $request->rubric_id;
        $shigarmalar = Shigarma::where('rubric_id',$category)->paginate(8);

        return new Category($shigarmalar);

    }
}
