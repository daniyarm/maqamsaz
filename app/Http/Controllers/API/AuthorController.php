<?php

namespace App\Http\Controllers\API;

use App\Models\Author;
use App\Models\Shigarma;
use App\Models\Orindaushi;
use Illuminate\Http\Request;
use  App\Http\Controllers\Controller;
use App\Http\Resources\Author as AuthorResource;
use App\Http\Resources\Orindaushi as OrindaushiResource;

class AuthorController extends Controller
{

    public function authorprofile(Request $request)
    {
        $author = Author::where('author_id',$request->author_id)->first();
        return new AuthorResource($author);
    }
    public function getorindaushiprofile(Request $request)
    {
        $author = Orindaushi::where('id',$request->orindaushi_id)->first();
        return new OrindaushiResource($author);
    }
}
