<?php

namespace App\Http\Controllers;

use App\Models\Author;
use App\Models\Shigarma;
use App\Models\Orindaushi;
use Illuminate\Http\Request;

class AuthorController extends Controller
{

    public function getauthorprofile($author_id)
    {
        $author = Author::where('author_id',$author_id)->first();
        $shigarmalar  =$author->shigarmalar()->paginate(12);
        return view('authorprofile' , compact('author','shigarmalar'));
    }
    public function getorindaushiprofile($orindaushi_id)
    {
        $author = Orindaushi::where('id',$orindaushi_id)->first();
        $shigarmalar  =$author->shigarmalar()->paginate(12);
        return view('orindaushiprofile' , compact('author','shigarmalar'));
    }
}
