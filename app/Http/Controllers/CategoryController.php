<?php

namespace App\Http\Controllers;

use App\Models\Rubric;
use App\Models\Shigarma;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function __construct()
    {

    }
    public function getcategory($rubric_id)
    {
       $rubric= Rubric::where('rubric_id',$rubric_id)->first();
        $shigarmalar = Shigarma::where('rubric_id',$rubric_id)->paginate(12);
        return view('category' , compact('shigarmalar', 'rubric'));
    }
    public function getshigarma($shigarma_id)
    {
        $shigarma = Shigarma::where('id',$shigarma_id)->first();
        return view('shigarma' , compact('shigarma'));
    }
}
