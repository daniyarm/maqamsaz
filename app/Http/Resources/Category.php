<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;


class Category extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'success' => true,
            'data' => $this->collection->transform(function($page){
                return [
                    'id' => $page->id,
                    'title' => $page->name,
                    'photo' => $page->photo,
                    'author' => $page->authors->transform(function($page){
                        return [
                            'fio' => $page->author_fio,
                        ];
                    }),
                    ];
                }),
        ];
    }
}
