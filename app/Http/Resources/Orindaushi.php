<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;


class Orindaushi extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'success'=>true,
            'id'=>$this->id,
            'fio'=>$this->name,
            'description' => $this->description,
            'photo' => $this->photo,
            'shigarmalar' => $this->shigarmalar->transform(function($page){
                return [
                    'id' => $page->id,
                    'name' => $page->name,
                    'photo' => $page->photo,
                    'author' => $page->authors->transform(function($page){
                        return [
                            'fio' => $page->author_fio,
                        ];
                    }),
                ];
            }),
        ];
    }
}
