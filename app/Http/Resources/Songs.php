<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Songs extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [

            'success'=>true,
            'name' => $this->name,
            'orindaushi' => $this->orindauwilar->transform(function($page){
                return [
                    'duration' => $page->pivot->duration,
                    'path' => url('/').$page->pivot->path,
                    'fio' => $page->name,
                ];
            }),


        ];
    }
}
