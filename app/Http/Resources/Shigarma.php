<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Shigarma extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'success'=>true,
                    'id' => $this->id,
                    'name' => $this->name,
                    'description' => $this->description,
                    'photo' => $this->photo,
                    'author' => $this->authors->transform(function($page){
                        return [
                            'fio' => $page->author_fio,
                            'author_id' => $page->author_id,
                        ];
                    }),
                    'orindaushi' => $this->orindauwilar->transform(function($page){
                        return [
                            'fio' => $page->name,
                            'orindaushi_id' => $page->id,
                        ];
                    }),
                    'rubric' => $this->rubric['rubric_name'],
                    'share' =>url('/').'/shigarma/'.$this->id


                ];

    }
}
