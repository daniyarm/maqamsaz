<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;


class Author extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'success'=>true,
            'author_id'=>$this->author_id,
            'author_fio'=>$this->author_fio,
            'author_description' => $this->author_description,
            'author_photo' => $this->author_photo,
            'shigarmalar' => $this->shigarmalar->transform(function($page){
                return [
                    'id' => $page->id,
                    'name' => $page->name,
                    'photo' => $page->photo,
                    'author' => $page->authors->transform(function($page){
                        return [
                            'fio' => $page->author_fio,
                        ];
                    }),
                ];
            }),
        ];
    }
}
