<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class ShigarmaAuthor extends Authenticatable
{

    protected $table = 'shigarma_authors';

    protected $fillable = [
        'shigarma_id', 'author_id',
    ];
}
