<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Author extends Authenticatable
{

    protected $table = 'author';
    protected $primaryKey = 'author_id';

    protected $fillable = [
        'author_fio', 'author_description', 'author_photo',
    ];

    public function shigarmalar()
    {
        return $this->belongsToMany('App\Models\Shigarma' ,'shigarma_authors' ,'author_id','shigarma_id');
    }
    public function getAuthorPhotoAttribute($value){
        return url('/').$value;
    }
}
