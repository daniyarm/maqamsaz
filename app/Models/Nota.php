<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Nota extends Authenticatable
{

    protected $table = 'shigarma_nota';

    protected $fillable = ['shigarma_id','path'];

    public function getPathAttribute($value){
        return url('/').$value;
    }

}
