<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Shigarma extends Authenticatable
{

    protected $table = 'shigarmalar';

    protected $fillable = [
        'name', 'description', 'photo','rubric_id'
    ];

    public function orindauwilar()
    {
        return $this->belongsToMany('App\Models\Orindaushi' ,'shigarma_orindaushi' ,'shigarma_id','orindaushi_id')
            ->withPivot('path','duration');
    }
    public function authors()
    {
        return $this->belongsToMany('App\Models\Author' ,'shigarma_authors' ,'shigarma_id','author_id');
    }
    public function rubric()
    {
        return $this->belongsTo('App\Models\Rubric', 'rubric_id');
    }
    public function notas()
    {
        return $this->hasOne('App\Models\Nota', 'shigarma_id');
    }
    public function birolen()
    {
        return $this->hasOne('App\Models\OrindaushiOleni', 'shigarma_id');
    }
    public function getPhotoAttribute($value){
        return url('/').$value;
    }
}
