<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Orindaushi extends Authenticatable
{

    protected $table = 'orindaushi';

    protected $fillable = ['name'];

    public function shigarmalar()
    {
        return $this->belongsToMany('App\Models\Shigarma', 'shigarma_orindaushi', 'orindaushi_id', 'shigarma_id');
    }
    public function getPhotoAttribute($value){
        return url('/').$value;
    }
}
