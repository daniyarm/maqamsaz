<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class OrindaushiOleni extends Authenticatable
{

    protected $table = 'shigarma_orindaushi';

    protected $fillable = [
        'shigarma_id', 'orindaushi_id', 'path', 'duration'
    ];

    public function shigarma()
    {
        return $this->belongsTo('App\Models\Shigarma', 'shigarma_id');
    }
    public function orindaushi()
    {
        return $this->belongsTo('App\Models\Orindaushi', 'orindaushi_id');
    }
    public function getPathAttribute($value)
    {
        return url('/').$value;
    }
}
