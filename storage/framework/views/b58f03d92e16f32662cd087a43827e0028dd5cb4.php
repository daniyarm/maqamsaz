

<?php $__env->startSection('css'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="page-wrapper" style="min-height: 319px;">
  <div class="container-fluid">
    <div class="row page-titles">
      <div class="col-md-8 col-8 align-self-center">
        <h3 class="text-themecolor m-b-0 m-t-0 d-inline-block">
          <a>Орындаушы</a>
        </h3>
      </div>
      <div class="col-md-4 col-4 align-self-center text-right">
        <a href="/admin/orindaushi/create" class="btn btn-success">Добавить</a>
      </div>
    </div>

    <div class="row white-bg">
      <div class="col-md-12">
        <div class="box-body">
          <div class="table-responsive">
            <table id="showed" class="table table-bordered table-striped">
              <thead>
                <tr style="border: 1px">
                  <th style="width: 30px">№</th>
                  <th>ФИО</th>
                  <th>фото</th>
                  <th></th>
                    <th></th>
                </tr>
              </thead>

              <tbody>
                <?php $__currentLoopData = $orindaushi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                  <td><?php echo e($value->id); ?></td>
                  <td><?php echo e($value->name); ?></td>
                  <td><img style="width:100px;" src="<?php echo e($value->photo); ?>"></td>
                  <td>
                    <a href="javascript:void(0)" onclick="remove(this,'<?php echo e($value->id); ?>','orindaushi')">
                      <i class="fas fa-trash"></i>
                    </a>
                  </td>
                    <td><a href="/admin/orindaushi/<?php echo e($value->id); ?>/edit"><i class="fas fa-pen"></i></a></td>
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </tbody>
            </table>
          </div>


        </div>
      </div>
    </div>
  </div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<script>
  $('#send').click(function () {
    $.post("/admin/orindaushi", {
      name: $('input[name=orindaushi_name]').val(),
      _token: '<?php echo e(csrf_token()); ?>'
    }, function (response) {
      $('tbody').append(
        "<tr><td>" + response.id + "</td>" +
        "<td>" + response.name + "</td>" +
        "<td><a href=\"javascript:void(0)\" onclick=\"remove(this,'" + response.id + "','orindaushi')\"><i class=\"fas fa-trash\"></i></a></td>" +
          " <td><a href=\"/admin/orindaushi/" + response.id +"/edit\"><i class=\"fas fa-pen\"></i></a></td></tr>"

      );
    });
  });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/www-root/data/www/maqamsaz.kz/resources/views/admin/orindaushi/orindaushi.blade.php ENDPATH**/ ?>