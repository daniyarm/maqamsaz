<?php
    $position = App\Models\Position::all();
?>
<?php $__currentLoopData = $position; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <?php if(!empty($news)): ?>
    <?php
        $news_pos = App\Models\NewsPosition::where('np_news_id', $news->news_id)->where('np_position_id', $item->position_id)->first();
    ?>
        <?php if(!empty($news_pos)): ?>
            <input class="form-check-input" id="item<?php echo e($item->position_id); ?>" type="checkbox" value="<?php echo e($item->position_id); ?>" name="news_position[]" checked>
            <label for="item<?php echo e($item->position_id); ?>"><?php echo e($item->position_name_ru); ?></label> 
        <?php else: ?>
            <input class="form-check-input" id="item<?php echo e($item->position_id); ?>" type="checkbox" value="<?php echo e($item->position_id); ?>" name="news_position[]">
            <label for="item<?php echo e($item->position_id); ?>"><?php echo e($item->position_name_ru); ?></label> 
        <?php endif; ?>      
    <?php elseif(!empty($banner)): ?>
    <?php
        $ban_pos = App\Models\BannerPosition::where('bp_banner_id', $banner->banner_id)->where('bp_position_id', $item->position_id)->first();
    ?>
        <?php if(!empty($ban_pos)): ?>
            <input class="form-check-input" id="item<?php echo e($item->position_id); ?>" type="checkbox" value="<?php echo e($item->position_id); ?>" name="news_position[]" checked>
            <label for="item<?php echo e($item->position_id); ?>"><?php echo e($item->position_name_ru); ?></label> 
        <?php else: ?>
            <input class="form-check-input" id="item<?php echo e($item->position_id); ?>" type="checkbox" value="<?php echo e($item->position_id); ?>" name="news_position[]">
            <label for="item<?php echo e($item->position_id); ?>"><?php echo e($item->position_name_ru); ?></label> 
        <?php endif; ?>      
    <?php else: ?>
        <input class="form-check-input" id="item<?php echo e($item->position_id); ?>" type="checkbox" value="<?php echo e($item->position_id); ?>" name="news_position[]">
        <label for="item<?php echo e($item->position_id); ?>"><?php echo e($item->position_name_ru); ?></label>    
    <?php endif; ?>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?><?php /**PATH /var/www/www-root/data/www/maqamsaz.kz/resources/views/admin/layouts/position.blade.php ENDPATH**/ ?>