
<?php $__env->startSection('content'); ?>
<section class="section-404">
    <div class="container">
        <img src="img/main/404.png" alt="">
        <h1>Бет табылмады</h1>
        <p>Парақ ескірген, жойылған немесе мүлде болған жоқ</p>
        <a href="/" class="btn-plain btn-orange">Басты бетке оралу</a>
    </div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/www-root/data/www/maqamsaz.kz/resources/views/errors/404.blade.php ENDPATH**/ ?>