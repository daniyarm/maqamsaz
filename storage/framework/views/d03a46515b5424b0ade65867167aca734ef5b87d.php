<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-133485315-2"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-133485315-2');
    </script>
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript" >
        (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
            m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
        (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

        ym(70719154, "init", {
            clickmap:true,
            trackLinks:true,
            accurateTrackBounce:true
        });
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/70719154" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width"/>
    <title>Maqamsaz</title>
    <link rel="stylesheet" href="/css/libs.min.css">
    <link rel="stylesheet" href="/css/all.css">
    <link rel="stylesheet" href="/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="/css/main.css">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo e(asset('/favicon.ico')); ?>">
    <?php echo $__env->yieldContent('style'); ?>
</head>
<body>
<?php echo $__env->make('admin.includes.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->yieldContent("content"); ?>
<footer>
    <div class="container">
        <div class="footer d-flex-justify">
            <a class="navbar-brand" href="/">
                <img src="/img/logo/logo.png" alt="">
            </a>
            <div class="copyright">
                <h3>"CaspianMediaServise" ЖШС © 2012-<?php echo date("Y"); ?></h3>
                <p>Материалды көшіріп басқанда Maqamsaz.kz сайтына гиперсілтеме міндетті түрде қойылуы тиіс.</p>
            </div>
            <div style="margin-right: 20px">
                <!-- Yandex.Metrika informer -->
                <a href="https://metrika.yandex.ru/stat/?id=70719154&amp;from=informer"
                   target="_blank" rel="nofollow"><img src="https://informer.yandex.ru/informer/70719154/3_1_FFFFFFFF_EFEFEFFF_0_pageviews"
                                                       style="width:88px; height:31px; border:0;" alt="Яндекс.Метрика" title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)" class="ym-advanced-informer" data-cid="70719154" data-lang="ru" /></a>
                <!-- /Yandex.Metrika informer -->
            </div>
            <div class="bg-box">
                <a href="https://bgpro.kz/" target="_blank">
                    <img src="/img/logo/copyright.png" alt="">
                </a>
            </div>
        </div>
    </div>
</footer>
<script src="/js/libs.min.js"></script>
<script>
    document.addEventListener('contextmenu', event => event.preventDefault());
    $('img').on("contextmenu", function(e) {
        if (e.target.nodeName === 'img') {
            //context menu attempt on top of an image element
            return false;
        }
    });
</script>
<?php echo $__env->yieldContent('script'); ?>
<script src="/js/common.js"></script>
</body>
</html>
<?php /**PATH C:\xampp\htdocs\maqamsaz.kz\resources\views/layouts/main.blade.php ENDPATH**/ ?>