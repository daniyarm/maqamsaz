

<?php $__env->startSection('css'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="page-wrapper" style="min-height: 319px;">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-8 col-8 align-self-center">
                    <h3 class="text-themecolor m-b-0 m-t-0">
                        Изменить рубрику
                    </h3>
                </div>
                <div class="col-md-4 col-4 align-self-center text-right">
                    <a href="/admin/rubric" class="btn btn-danger">Назад</a>
                </div>
            </div>
            <div class="row">
                <form class="col-lg-12 col-md-12 row" action="/admin/rubric/<?php echo e($rubric->rubric_id); ?>" method="POST" enctype="multipart/form-data" >
                    <?php echo csrf_field(); ?>
                    <?php echo method_field("put"); ?>
                    <div class="col-lg-8 col-md-12">
                        <div class="card">
                            <div class="card-block">
                                <?php if($errors->any()): ?>
                                    <div class="alert alert-danger">
                                        <ul>
                                            <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <li><?php echo e($error); ?></li>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </ul>
                                    </div>
                                <?php endif; ?>
                                <div class="box-body">
                                    <div class="form-group">
                                        <label>Название</label>
                                        <input type="text" class="form-control" name="name" value="<?php echo e($rubric->rubric_name); ?>" placeholder="" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12">
                        <div class="card">
                            <div class="card-block">
                                <div class="box box-primary" style="padding: 30px; text-align: center">
                                    <div class="tab-pane fade show active" id="img-ru" role="tabpanel"
                                         aria-labelledby="img-ru-tab">
                                        <div style="padding: 20px; border: 1px solid #c2e2f0">
                                            <img class="image-src" id="blah1" src="<?php echo e($rubric->photo); ?>"
                                                 style="width: 100%; " />
                                        </div>
                                        <div style="background-color: #c2e2f0;height: 40px;margin: 0 auto;width: 2px;">
                                        </div>
                                        <label class="btn btn-primary label-img" for="imgInp1">
                                            <input id="imgInp1" type="file" name="photo" class="d-none">
                                            <i class="fa fa-plus"></i>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-12 text-right">
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Сохранить</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    inpid = $(input).attr('id');
                    inpid = inpid[inpid.length - 1];
                    imgId = '#blah' + inpid;
                    $(imgId).attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imgInp1").change(function () {
            readURL(this);
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/www-root/data/www/maqamsaz.kz/resources/views/admin/rubric/rubric-edit.blade.php ENDPATH**/ ?>