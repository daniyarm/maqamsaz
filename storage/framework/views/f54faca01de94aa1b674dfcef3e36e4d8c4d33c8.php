<?php $__env->startSection('style'); ?>
    <link rel="stylesheet" href="/css/audioplayer.css">
    <link rel="stylesheet" href="/css/lg-transitions.min.css">
    <link rel="stylesheet" href="/css/lightgallery.min.css">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <section class="category">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="/">Басты бет</a></li>
                <li><a href="/category/<?php echo e($shigarma->rubric["rubric_id"]); ?>"><?php echo e($shigarma->rubric["rubric_name"]); ?></a></li>
                <li class="active"><?php echo e($shigarma->name); ?></li>
            </ol>
            <div class="row">
                <div class="col-md-3 col-sm-4">
                    <div class="inside-img">
                        <img src="<?php echo e($shigarma->photo); ?>" alt="">
                    </div>
                    <?php if($shigarma->notas): ?>
                    <div class="inside-btn d-flex-justify">
                        <img src="/img/icon/note.png">
                        <p>Нота</p>
                        <button class="btn-plain dynamic"><a target="blank" href="<?php echo e(($shigarma->notas)?$shigarma->notas["path"]:'#'); ?>"><img src="/img/icon/arr-white.png" alt=""></a></button>
                    </div>
                    <?php endif; ?>
                </div>
                <div class="col-md-9 col-sm-8">
                    <div class="inside-caption">
                        <h2><?php echo e($shigarma->name); ?></h2>
                        <?php echo $shigarma->description; ?>

                        <div class="inside-info">
                            <p>
                                <span>Автор:</span>
                                <?php $__currentLoopData = $shigarma->authors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($key == count($shigarma->authors) - 1): ?>
                                        <a href="/author/<?php echo e($item->author_id); ?>"><?php echo e($item->author_fio); ?></a>
                                    <?php else: ?>
                                        <a href="/author/<?php echo e($item->author_id); ?>"><?php echo e($item->author_fio); ?></a> ,
                                    <?php endif; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </p>
                            <p>
                                <span>Орындаушы:</span>
                                <?php $__currentLoopData = $shigarma->orindauwilar; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($key == count($shigarma->orindauwilar) - 1): ?>
                                        <a href="/orindaushi/<?php echo e($item->id); ?>"><?php echo e($item->name); ?></a>
                                    <?php else: ?>
                                        <a href="/orindaushi/<?php echo e($item->id); ?>"><?php echo e($item->name); ?></a> ,
                                    <?php endif; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </p>
                        </div>
                    </div>
                    <div class="audio-box">
                        <?php $__currentLoopData = $shigarma->orindauwilar; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="audio-cover">
                                <audio preload="auto" controls>
                                    <source src="<?php echo e($item->pivot["path"]); ?>">
                                </audio>

                                <!--<audio src="audio.wav" preload="auto" controls autoplay loop></audio>-->
                                <!--<br type="_moz">-->
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    <script src="/js/audioplayer.js"></script>
    <script src="/js/lightgallery.min.js"></script>
    <script src="/js/lightgallery-all.min.js"></script>
    <script type="text/javascript">
        /*$('.dynamic').on('click', function() {

            $(this).lightGallery({
                dynamic: true,
                dynamicEl: [{
                    "src": '/img/main/bobek.png',
                    'thumb': '/img/main/bobek.png',
                    'subHtml': '<h4>Fading Light</h4><p>Classic view from Rigwood Jetty on Coniston Water an old archive shot similar to an old post but a little later on.</p>'
                }, {
                    'src': '/img/main/bobek.png',
                    'thumb': '/img/main/bobek.png',
                    'subHtml': "<h4>Bowness Bay</h4><p>A beautiful Sunrise this morning taken En-route to Keswick not one as planned but I'm extremely happy I was passing the right place at the right time....</p>"
                }, {
                    'src': '/img/main/bobek.png',
                    'thumb': '/img/main/bobek.png',
                    'subHtml': "<h4>Coniston Calmness</h4><p>Beautiful morning</p>"
                }]
            })

        });*/
        //    $('.note').lightGallery();
        $(function() {
            $('audio').audioPlayer();
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\maqamsaz.kz\resources\views/shigarma.blade.php ENDPATH**/ ?>