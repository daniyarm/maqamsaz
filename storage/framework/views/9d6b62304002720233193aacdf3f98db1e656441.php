
<?php $__env->startSection('css'); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="page-wrapper" style="min-height: 319px;">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-8 col-8 align-self-center">
                    <h3 class="text-themecolor m-b-0 m-t-0">
                        Добавить олен
                    </h3>
                </div>
                <div class="col-md-4 col-4 align-self-center text-right">
                    <a href="/admin/olen" class="btn btn-danger">Назад</a>
                </div>
            </div>
            <div class="row">
                <form class="col-lg-12 col-md-12 row" action="/admin/olen" method="POST" enctype="multipart/form-data">
                    <?php echo csrf_field(); ?>
                    <div class="col-lg-12 col-md-12">
                        <div class="card">
                            <div class="card-block">
                                <?php if($errors->any()): ?>
                                    <div class="alert alert-danger">
                                        <ul>
                                            <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <li><?php echo e($error); ?></li>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </ul>
                                    </div>
                                <?php endif; ?>
                                <div class="box-body">
                                    <div class="form-group">
                                        <label>Шагырма</label>
                                        <select class="form-control" name="shigarma_id">
                                            <?php $__currentLoopData = $shigarmalar; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($item->id); ?>"><?php echo e($item->name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Орындаушы</label>
                                        <select class="form-control" name="orindaushi_id">
                                            <?php $__currentLoopData = $orindaushi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($item->id); ?>"><?php echo e($item->name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                    </div>
                                    <div class="form-group files" id="files1">
                                    <span class="btn btn-success btn-file">
                                        Добавить файл
                                        <input type="file" name="archive_file" />
                                    </span>
                                        <br />
                                        <ul class="fileList m-t-20">
                                            <?php if(!empty($archive)): ?>
                                                <?php
                                                    $pice = str_replace("'", "", substr($archive->archive_file, 0, -1));
                                                    $links = explode(",", $pice);
                                                ?>
                                                <?php $__currentLoopData = $links; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <li>
                                                        <strong><?php echo e($item); ?></strong>&nbsp; &nbsp;
                                                        <a class="removeFile text-danger" href="#" data-fileid="0">✕</a>
                                                    </li>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php endif; ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-12 text-right">
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Сохранить</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
    <script src="/js/fileupload.js"></script>
    <script>
        $("#files1").fileUploader(filesToUpload);
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/www-root/data/www/maqamsaz.kz/resources/views/admin/olen/olen-create.blade.php ENDPATH**/ ?>