

<?php $__env->startSection('css'); ?>
<style>
    .active-top-show {
        background-color: #fff !important;
    }

    #nav-tabContent {
        padding-top: 10px;
    }
</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="page-wrapper" style="min-height: 319px;">
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-8 col-8 align-self-center">
                <h3 class="text-themecolor m-b-0 m-t-0">
                    Добавить новость
                </h3>
            </div>
            <div class="col-md-4 col-4 align-self-center text-right">
                <a href="/admin/news" class="btn btn-danger">Назад</a>
            </div>
        </div>
        <div class="row">
            <?php if(empty($news)): ?>
            <form class="col-lg-12 col-md-12 row" action="/admin/news" method="POST" enctype="multipart/form-data">
            <?php else: ?>
            <form class="col-lg-12 col-md-12 row" action="/admin/news/<?php echo e($news->news_id); ?>" method="POST" enctype="multipart/form-data">
                <?php echo method_field('PUT'); ?> 
            <?php endif; ?>
                <?php echo csrf_field(); ?>
                <div class="col-lg-8 col-md-12">
                    <div class="card">
                        <div class="card-block">
                            <?php if($errors->any()): ?>
                            <div class="alert alert-danger">
                                <ul>
                                    <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li><?php echo e($error); ?></li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            </div>
                            <?php endif; ?>
                            <div class="box-body">
                                <nav>
                                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                        <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab"
                                            href="#nav-home" role="tab" aria-controls="nav-home"
                                            aria-selected="true">Русский</a>
                                        <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab"
                                            href="#nav-profile" role="tab" aria-controls="nav-profile"
                                            aria-selected="false">Казахский</a>
                                        <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab"
                                            href="#nav-contact" role="tab" aria-controls="nav-contact"
                                            aria-selected="false">Английский</a>
                                    </div>
                                </nav>
                                <div class="tab-content" id="nav-tabContent">
                                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel"
                                        aria-labelledby="nav-home-tab">
                                        <div class="form-group">
                                            <label>Название</label>
                                            <input type="text" class="form-control" name="news_name_ru" value="<?php echo e(!empty($news) ? $news->news_name_ru : old('news_name_ru')); ?>" />
                                        </div>
                                        <div class="form-group">
                                            <label>Краткое описание</label>
                                            <textarea name="news_short_desc_ru"
                                                class="form-control"><?php echo e(!empty($news) ? $news->news_short_desc_ru : old('news_short_desc_ru')); ?></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Описание</label>
                                            <textarea id="editor" name="news_desc_ru"><?php echo e(!empty($news) ? $news->news_desc_ru : old('news_desc_ru')); ?></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Meta SEO description </label>
                                            <input type="text" class="form-control"
                                                name="news_meta_description_ru" value="<?php echo e(!empty($news) ? $news->news_meta_description_ru : old('news_meta_description_ru')); ?>" />
                                        </div>
                                        <div class="form-group">
                                            <label>Meta SEO keywords</label>
                                            <input type="text" class="form-control"
                                                name="news_meta_keywords_ru" value="<?php echo e(!empty($news) ? $news->news_meta_keywords_ru : old('news_meta_keywords_ru')); ?>" />
                                        </div>
                                        <div class="form-group">
                                            <label>Тэги (через запятую)</label>
                                            <textarea name="tag_ru" class="form-control"><?php echo e(!empty($news) ? $news->tag_ru : old('tag_ru')); ?></textarea>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="nav-profile" role="tabpanel"
                                        aria-labelledby="nav-profile-tab">
                                        <div class="form-group">
                                            <label>Название</label>
                                            <input type="text" class="form-control" name="news_name_kz" value="<?php echo e(!empty($news) ? $news->news_name_kz : old('news_name_kz')); ?>"/>
                                        </div>
                                        <div class="form-group">
                                            <label>Краткое описание</label>
                                            <textarea name="news_short_desc_kz"
                                                class="ckeditor form-control"><?php echo e(!empty($news) ? $news->news_short_desc_kz : old('news_short_desc_kz')); ?></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Описание</label>
                                            <textarea id="editor1" name="news_desc_kz"><?php echo e(!empty($news) ? $news->news_desc_kz : old('news_desc_kz')); ?></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Meta SEO description </label>
                                            <input type="text" class="form-control"
                                                name="news_meta_description_kz" value="<?php echo e(!empty($news) ? $news->news_meta_description_kz : old('news_meta_description_kz')); ?>"/>
                                        </div>
                                        <div class="form-group">
                                            <label>Meta SEO keywords</label>
                                            <input type="text" class="form-control"
                                                name="news_meta_keywords_kz" value="<?php echo e(!empty($news) ? $news->news_meta_keywords_kz : old('news_meta_keywords_kz')); ?>"/>
                                        </div>
                                        <div class="form-group">
                                            <label>Тэги (через запятую)</label>
                                            <textarea name="tag_kz" class="form-control"><?php echo e(!empty($news) ? $news->tag_kz : old('tag_kz')); ?></textarea>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="nav-contact" role="tabpanel"
                                        aria-labelledby="nav-contact-tab">
                                        <div class="form-group">
                                            <label>Название</label>
                                            <input type="text" class="form-control" name="news_name_en" value="<?php echo e(!empty($news) ? $news->news_name_en : old('news_name_en')); ?>"/>
                                        </div>
                                        <div class="form-group">
                                            <label>Краткое описание</label>
                                            <textarea name="news_short_desc_en"
                                                class="ckeditor form-control"><?php echo e(!empty($news) ? $news->news_short_desc_en : old('news_short_desc_en')); ?></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Описание</label>
                                            <textarea id="editor2" name="news_desc_en"><?php echo e(!empty($news) ? $news->news_desc_en : old('news_desc_en')); ?></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Meta SEO description </label>
                                            <input type="text" class="form-control"
                                                name="news_meta_description_en" value="<?php echo e(!empty($news) ? $news->news_meta_description_en : old('news_meta_description_en')); ?>"/>
                                        </div>
                                        <div class="form-group">
                                            <label>Meta SEO keywords</label>
                                            <input type="text" class="form-control"
                                                name="news_meta_keywords_en" value="<?php echo e(!empty($news) ? $news->news_meta_keywords_en : old('news_meta_keywords_en')); ?>"/>
                                        </div>
                                        <div class="form-group">
                                            <label>Тэги (через запятую)</label>
                                            <textarea name="tag_en" class="form-control"><?php echo e(!empty($news) ? $news->tag_en : old('tag_en')); ?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Автор</label>
                                    <select class="form-control" name="author_id">
                                        <?php echo $__env->make('admin.layouts.users', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Язык</label><br>
                                    <input class="form-check-input" id="ru" type="checkbox" value="ru"
                                        name="news_lang[]" <?php echo e((!empty($news) && strpos($news->news_lang, 'ru') !== false) ? 'checked' : ""); ?>>
                                    <label for="ru">Русский</label>
                                    <input class="form-check-input" id="kz" type="checkbox" value="kz"
                                        name="news_lang[]" <?php echo e((!empty($news) && strpos($news->news_lang, 'kz') !== false) ? 'checked' : ""); ?>>
                                    <label for="kz">Казахский</label>
                                    <input class="form-check-input" id="en" type="checkbox" value="en"
                                        name="news_lang[]" <?php echo e((!empty($news) && strpos($news->news_lang, 'en') !== false) ? 'checked' : ""); ?>>
                                    <label for="en">Английский</label>
                                </div>
                                <div class="form-group">
                                    <label>Отображать</label><br>
                                    <input type="radio" class="form-check-input" name="is_show" value="1"
                                        id="is_show_yes" <?php echo e((!empty($news) && $news->is_show == 1) ? "checked" : "checked"); ?>>
                                    <label class="form-check-label" for="is_show_yes">
                                        Да
                                    </label>
                                    <input type="radio" class="form-check-input" name="is_show" value="0"
                                        id="is_show_no" <?php echo e((!empty($news) && $news->is_show == 0) ? "checked" : ""); ?>>
                                    <label class="form-check-label" for="is_show_no">
                                        Нет
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label>Рубрика</label>
                                    <select class="form-control" name="news_rubric_id">
                                        <?php echo $__env->make('admin.layouts.rubric', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Позиция</label><br>
                                    <?php echo $__env->make('admin.layouts.position', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                                </div>
                                <div class="form-group">
                                    <label>Дата</label>
                                    <input type="date" class="form-control" name="news_date" value="<?php echo e(!empty($news) ? $news->news_date : old('news_date')); ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12">
                    <div class="card">
                        <div class="card-block">
                            <div class="box box-primary" style="padding: 30px; text-align: center">
                                <nav>
                                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                        <a class="nav-item nav-link active" id="img-ru-tab" data-toggle="tab"
                                            href="#img-ru" role="tab" aria-controls="img-ru"
                                            aria-selected="true">Рус</a>
                                        <a class="nav-item nav-link" id="img-kz-tab" data-toggle="tab" href="#img-kz"
                                            role="tab" aria-controls="img-kz" aria-selected="false">Каз</a>
                                        <a class="nav-item nav-link" id="img-en-tab" data-toggle="tab" href="#img-en"
                                            role="tab" aria-controls="img-en" aria-selected="false">Анг</a>
                                    </div>
                                </nav>
                                <div class="tab-content" id="nav-tabContent">
                                    <div class="tab-pane fade show active" id="img-ru" role="tabpanel"
                                        aria-labelledby="img-ru-tab">
                                        <div style="padding: 20px; border: 1px solid #c2e2f0">
                                            <img class="image-src" id="blah1" src="<?php echo e(!empty($news) ? $news->news_image_ru : old('news_image_ru')); ?>"
                                                style="width: 100%; " />
                                        </div>
                                        <div style="background-color: #c2e2f0;height: 40px;margin: 0 auto;width: 2px;">
                                        </div>
                                        <label class="btn btn-primary label-img" for="imgInp1">
                                            <input id="imgInp1" type="file" name="news_image_ru" class="d-none">
                                            <i class="fa fa-plus"></i>
                                        </label>
                                    </div>
                                    <div class="tab-pane fade" id="img-kz" role="tabpanel" aria-labelledby="img-kz-tab">
                                        <div style="padding: 20px; border: 1px solid #c2e2f0">
                                            <img class="image-src" id="blah2" src="<?php echo e(!empty($news) ? $news->news_image_kz : old('news_image_kz')); ?>"
                                                style="width: 100%; " />
                                        </div>
                                        <div style="background-color: #c2e2f0;height: 40px;margin: 0 auto;width: 2px;">
                                        </div>
                                        <label class="btn btn-primary label-img" for="imgInp2">
                                            <input id="imgInp2" type="file" name="news_image_kz" class="d-none">
                                            <i class="fa fa-plus"></i>
                                        </label>
                                    </div>
                                    <div class="tab-pane fade" id="img-en" role="tabpanel" aria-labelledby="img-en-tab">
                                        <div style="padding: 20px; border: 1px solid #c2e2f0">
                                            <img class="image-src" id="blah3" src="<?php echo e(!empty($news) ? $news->news_image_en : old('news_image_en')); ?>"
                                                style="width: 100%; " />
                                        </div>
                                        <div style="background-color: #c2e2f0;height: 40px;margin: 0 auto;width: 2px;">
                                        </div>
                                        <label class="btn btn-primary label-img" for="imgInp3">
                                            <input id="imgInp3" type="file" name="news_image_en" class="d-none">
                                            <i class="fa fa-plus"></i>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-md-12 text-right">
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Сохранить</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                inpid = $(input).attr('id');
                inpid = inpid[inpid.length - 1];
                imgId = '#blah' + inpid;
                $(imgId).attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#imgInp1").change(function () {
        readURL(this);
    });
    $("#imgInp2").change(function () {
        readURL(this);
    });
    $("#imgInp3").change(function () {
        readURL(this);
    });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/www-root/data/www/maqamsaz.kz/resources/views/admin/news/edit-news.blade.php ENDPATH**/ ?>