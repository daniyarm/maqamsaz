

<?php $__env->startSection('css'); ?>
<style>
td img{
  width: 85px;
  height: 85px;
}
</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="page-wrapper" style="min-height: 319px;">
  <div class="container-fluid">
    <div class="row page-titles">
      <div class="col-md-8 col-8 align-self-center">
        <h3 class="text-themecolor m-b-0 m-t-0 d-inline-block">
          <a>Архив</a>
        </h3>
      </div>
      <div class="col-md-4 col-4 align-self-center text-right">
        <a href="/admin/archive/create" class="btn btn-success">Добавить</a>
      </div>
    </div>

    <div class="row white-bg">
      <div class="col-md-12">
        <div class="box-body">
          <div class="table-responsive">
            <table id="showed" class="table table-bordered table-striped">
              <thead>
                <tr style="border: 1px">
                  <th style="width: 30px">№</th>
                  <th>Картинка</th>
                  <th>Файл</th>
                  <th></th>
                  <th></th>
                </tr>
              </thead>

              <tbody>
                <?php $__currentLoopData = $archive; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                  <td><?php echo e($value->archive_id); ?></td>
                  <td><img src="<?php echo e($value->archive_image); ?>" alt=""></td>
                  <?php
                      $pice = str_replace("'", "", $value->archive_file);
                      $links = explode(",", $pice);
                  ?>
                  <td>
                    <?php $__currentLoopData = $links; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <?php if($loop->last): ?>
                        <a href="<?php echo e($item); ?>" target="_blank"><?php echo e($item); ?></a>
                      <?php else: ?>
                        <a href="<?php echo e($item); ?>" target="_blank"><?php echo e($item); ?></a>,
                      <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </td>
                  <td>
                    <a href="javascript:void(0)" onclick="remove(this,'<?php echo e($value->archive_id); ?>','archive')">
                      <i class="fas fa-trash"></i>
                    </a>
                  </td>
                  <td><a href="/admin/archive/<?php echo e($value->archive_id); ?>/edit"><i class="fas fa-pen"></i></a></td>
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/www-root/data/www/maqamsaz.kz/resources/views/admin/archive/archive.blade.php ENDPATH**/ ?>