<?php
    $position = App\Models\Position::all();
?>
    <option selected>Выберите</option>
<?php $__currentLoopData = $position; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <?php if(!empty($banner)): ?>
    <option value="<?php echo e($item->position_id); ?>" <?php echo e(($banner->banner_position_id == $item->position_id) ? "selected" : ""); ?>><?php echo e($item->position_name_ru); ?></option>
    <?php elseif(!empty($slider)): ?>
    <option value="<?php echo e($item->position_id); ?>" <?php echo e(($slider->slider_position == $item->position_id) ? "selected" : ""); ?>><?php echo e($item->position_name_ru); ?></option>
    <?php else: ?>
    <option value="<?php echo e($item->position_id); ?>"><?php echo e($item->position_name_ru); ?></option>
    <?php endif; ?>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?><?php /**PATH /var/www/www-root/data/www/maqamsaz.kz/resources/views/admin/layouts/select-position.blade.php ENDPATH**/ ?>