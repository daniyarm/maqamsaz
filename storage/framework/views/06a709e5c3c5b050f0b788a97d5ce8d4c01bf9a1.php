<?php
    $rubric = App\Models\Rubric::all();
?>
    <option selected>Выберите</option>
<?php $__currentLoopData = $rubric; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <?php if(!empty($banner)): ?>
        <option value="<?php echo e($item->rubric_id); ?>" <?php echo e(($banner->banner_rubric_id == $item->rubric_id) ? "selected" : ""); ?>><?php echo e($item->rubric_name_ru); ?></option>
    <?php elseif(!empty($news)): ?>
        <option value="<?php echo e($item->rubric_id); ?>" <?php echo e(($news->news_rubric_id == $item->rubric_id) ? "selected" : ""); ?>><?php echo e($item->rubric_name_ru); ?></option>
    <?php else: ?>
        <option value="<?php echo e($item->rubric_id); ?>"><?php echo e($item->rubric_name_ru); ?></option>
    <?php endif; ?>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?><?php /**PATH /var/www/www-root/data/www/maqamsaz.kz/resources/views/admin/layouts/rubric.blade.php ENDPATH**/ ?>