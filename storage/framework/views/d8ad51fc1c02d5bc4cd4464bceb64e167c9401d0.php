<?php $__env->startSection('content'); ?>
    <section class="main">
        <div class="main-bg">
            <img src="<?php echo e($sliders[0]["slider_image"]); ?>" alt="">
        </div>
        <div class="container">
            <div class="main-caption">
                
                <img src="/img/main/404.png" alt="">
            </div>
        </div>
    </section>
    <section class="type bg-grey">
        <div class="container">
            <div class="type-cover">
                <h2>Бөлімдер</h2>
                <div class="row">
                    <?php $__currentLoopData = $rubrics; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="col-md-4 col-sm-4">
                            <a href="/category/<?php echo e($item->rubric_id); ?>">
                                <div class="type-item">
                                    <div class="type-item-img">
                                        <img src="<?php echo e($item->photo); ?>" alt="">
                                    </div>
                                    <div class="type-item-caption">
                                        <h3>
                                            <?php echo e($item->rubric_name); ?>

                                        </h3>
                                    </div>
                                </div>
                            </a>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
            <div class="description-base">
                <div class="row">
                    <div class="col-md-3 col-sm-4">
                        <div class="description-base-img">
                            <img src="img/main/logo-lg.png" alt="">
                        </div>
                    </div>
                    <div class="col-md-9 col-sm-8">
                        <div class="description-base-caption">
                            <h2><?php echo e($sliders[1]["header"]); ?></h2>
                            <p>
                                <?php echo e($sliders[1]["slider_text"]); ?>

                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\maqamsaz.kz\resources\views/index.blade.php ENDPATH**/ ?>