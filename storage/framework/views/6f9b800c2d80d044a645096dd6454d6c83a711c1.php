

<?php $__env->startSection('css'); ?>
    <link rel="stylesheet" href="/css/image-uploader.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="page-wrapper" style="min-height: 319px;">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="col-md-8 col-8 align-self-center">
                    <h3 class="text-themecolor m-b-0 m-t-0">
                        Добавить шығарма
                    </h3>
                </div>
                <div class="col-md-4 col-4 align-self-center text-right">
                    <a href="/admin/shigarma" class="btn btn-danger">Назад</a>
                </div>
            </div>
            <div class="row">

                            <form class="col-lg-12 col-md-12 row" action="/admin/shigarma/<?php echo e($shigarma->id); ?>" method="POST" enctype="multipart/form-data">
                                <?php echo method_field('PUT'); ?>

                                <?php echo csrf_field(); ?>
                                <div class="col-lg-8 col-md-12">
                                    <div class="card">
                                        <div class="card-block">
                                            <?php if($errors->any()): ?>
                                                <div class="alert alert-danger">
                                                    <ul>
                                                        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <li><?php echo e($error); ?></li>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </ul>
                                                </div>
                                            <?php endif; ?>
                                            <div class="box-body">
                                                <div class="form-group">
                                                    <label>Название</label>
                                                    <input type="text" class="form-control" name="name" value="<?php echo e($shigarma->name); ?>" />
                                                </div>
                                                <div class="form-group">
                                                    <label>Описание</label>
                                                    <textarea id="editor" name="description"><?php echo e($shigarma->description); ?></textarea>
                                                </div>
                                                <div class="form-group">
                                                    <label>Рубрика</label>
                                                    <select class="form-control" name="rubric_id">
                                                        <?php $__currentLoopData = $rubrics; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <option <?php echo e(($item->rubric_id == $shigarma->rubric_id)?'selected':''); ?> value="<?php echo e($item->rubric_id); ?>"><?php echo e($item->rubric_name); ?></option>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </select>
                                                </div>

                                                <?php if($shigarma->notas): ?>
                                                    <a download href="<?php echo e($shigarma->notas["path"]); ?>">Скачать</a>
                                                <?php endif; ?>
                                                <br>
                                                <div class="form-group">
                                                    <label>Нота</label>
                                                    <input type="file" name="images" class="form-control">
                                                </div>

                                                <div class="form-group">
                                                <input name="authors" placeholder="Авторларды тандаңыз"
                                                      value="
                                                                        <?php $__currentLoopData = $shigarma->authors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $author): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                            <?php echo e($author->author_fio); ?>,
                                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                               ">
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-12">
                                    <div class="card">
                                        <div class="card-block">
                                            <div class="box box-primary" style="padding: 30px; text-align: center">
                                                    <div class="tab-pane fade show active" id="img-ru" role="tabpanel"
                                                         aria-labelledby="img-ru-tab">
                                                        <div style="padding: 20px; border: 1px solid #c2e2f0">
                                                            <img class="image-src" id="blah1" src="<?php echo e($shigarma->photo); ?>"
                                                                 style="width: 100%; " />
                                                        </div>
                                                        <div style="background-color: #c2e2f0;height: 40px;margin: 0 auto;width: 2px;">
                                                        </div>
                                                        <label class="btn btn-primary label-img" for="imgInp1">
                                                            <input id="imgInp1" type="file" name="photo" class="d-none">
                                                            <i class="fa fa-plus"></i>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-8 col-md-12 text-right">
                                    <div class="box-footer">
                                        <button type="submit" class="btn btn-primary">Сохранить</button>
                                    </div>
                                </div>
                            </form>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>

    <script type="text/javascript" src="/js/image-uploader.min.js"></script>



    <script>
        var array=[];
        <?php $__currentLoopData = $authors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        array.push("<?php echo e($item->author_fio); ?>");
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        $(document).ready(function() {
            $('[name=authors]').tagify({
                delimiters: ",",
                pattern: null,
                maxTags: Infinity,
                callbacks: {},
                addTagOnBlur: true,
                duplicates: false,
                whitelist: array,
                blacklist: [],
                enforceWhitelist: true,
                autoComplete: true
            });
        });
    </script>
    <script>

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    inpid = $(input).attr('id');
                    inpid = inpid[inpid.length - 1];
                    imgId = '#blah' + inpid;
                    $(imgId).attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imgInp1").change(function () {
            readURL(this);
        });
        $("#imgInp2").change(function () {
            readURL(this);
        });
        $("#imgInp3").change(function () {
            readURL(this);
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/www-root/data/www/maqamsaz.kz/resources/views/admin/shigarma/shigarma-edit.blade.php ENDPATH**/ ?>