
<?php $__env->startSection('content'); ?>
<section class="category search-box">
    <div class="container">
        <h1><span>Iздеу нәтижесі:</span> <?php echo e($search); ?></h1>

        <div class="row">
            <?php $__currentLoopData = $shigarmalar; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="col-md-2 col-sm-3">
                    <div class="category-item">
                        <div class="category-item-img">
                            <img src="<?php echo e($item->photo); ?>" alt="">
                            <div class="overlay-play">
                                <div class="play-info">
                                    <img src="/img/icon/play.png" alt="">
                                    <span>05:31</span>
                                </div>
                                <a href="/shigarma/<?php echo e($item->id); ?>" class="btn-plain play-lg">
                                    <img src="/img/icon/play-lg.png" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="category-item-caption">
                            <h3><?php echo e($item->name); ?></h3>
                            <?php $__currentLoopData = $item->authors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <p><?php echo e($item->author_fio); ?></p>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                    </div>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
        <?php echo e($shigarmalar->links()); ?>

    </div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/www-root/data/www/maqamsaz.kz/resources/views/search.blade.php ENDPATH**/ ?>