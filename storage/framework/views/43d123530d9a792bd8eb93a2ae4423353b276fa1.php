

<?php $__env->startSection('css'); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="page-wrapper" style="min-height: 319px;">
  <div class="container-fluid">
    <div class="row page-titles">
      <div class="col-md-8 col-8 align-self-center">
        <h3 class="text-themecolor m-b-0 m-t-0 d-inline-block">
          <a>Статичный страницы</a>
        </h3>
      </div>
      <div class="col-md-4 col-4 align-self-center text-right">
        <a href="/admin/pages/create" class="btn btn-success">Добавить</a>
      </div>
    </div>
    <div class="row page-titles">
      <div class="col-md-12 col-12 align-self-center">
        <h5 class="text-themecolor m-b-0 m-t-0 d-inline-block active-top-show">
          <a class="show">Опубликованные страницы</a>
          </h3>
          <h5 class="text-themecolor m-b-0 m-t-0 m-l-20 d-inline-block">
            <a class="not-show">Неопубликованные страницы</a>
          </h5>
          <div class="clear-float"></div>
      </div>
    </div>

    <div class="row white-bg">
      <div class="col-md-12">
        <div class="box-body">
          <div class="table-responsive">
            <table id="showed" class="table table-bordered table-striped">
              <thead>
                <tr style="border: 1px">
                  <th style="width: 30px">№</th>
                  <th>Название</th>
                  <th></th>
                  <th></th>
                </tr>
              </thead>

              <tbody>
                <?php $__currentLoopData = $page; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                  <td><?php echo e($value->page_id); ?></td>
                  <td><?php echo e($value->page_name_ru); ?></td>
                  <td>
                    <a href="javascript:void(0)" onclick="remove(this,'<?php echo e($value->page_id); ?>','pages')">
                      <i class="fas fa-trash"></i>
                    </a>
                  </td>
                  <td><a href="/admin/pages/<?php echo e($value->page_id); ?>/edit"><i class="fas fa-pen"></i></a></td>
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </tbody>
            </table>
          </div>

          <div class="table-responsive">
            <table id="not-showed" class="table table-bordered table-striped">
              <thead>
                <tr style="border: 1px">
                  <th style="width: 30px">№</th>
                  <th>Название</th>
                  <th></th>
                  <th></th>
                </tr>
              </thead>

              <tbody>
                <?php $__currentLoopData = $page_not_show; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                  <td><?php echo e($value->page_id); ?></td>
                  <td><?php echo e($value->page_name_ru); ?></td>
                  <td>
                    <a href="javascript:void(0)" onclick="remove(this,'<?php echo e($value->page_id); ?>','pages')">
                      <i class="fas fa-trash"></i>
                    </a>
                  </td>
                  <td><a href="/admin/pages/<?php echo e($value->page_id); ?>/edit"><i class="fas fa-pen"></i></a></td>
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/www-root/data/www/maqamsaz.kz/resources/views/admin/pages/pages.blade.php ENDPATH**/ ?>