

<?php $__env->startSection('css'); ?>
    
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="page-wrapper" style="min-height: 319px;">
    <div class="container-fluid">
      <div class="row page-titles">
        <div class="col-md-8 col-8 align-self-center">
          <h3 class="text-themecolor m-b-0 m-t-0 d-inline-block">
            <a>Подписка</a>
          </h3>
        </div>
      </div>
  
      <div class="row white-bg">
        <div class="col-md-12">
          <div class="box-body">
            <table id="showed" class="table table-bordered table-striped">
              <thead>
                <tr style="border: 1px">
                  <th style="width: 30px">№</th>
                  <th>Email</th>
                  <th>Рубрика</th>
                  <th>Статус</th>
                  <th></th>
                </tr>
              </thead>
  
              <tbody>
                  <?php $__currentLoopData = $subscription; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <tr>
                          <td><?php echo e($value->subscription_id); ?></td>
                          <td><?php echo e($value->subscription_user_email); ?></td>
                          <td><?php echo e($value->rubric_name_ru); ?></td>
                          <td><?php echo e($value->subscription_status); ?></td>
                          <td>
                            <a href="javascript:void(0)" onclick="remove(this,'<?php echo e($value->subscription_id); ?>','subscription')">
                              <i class="fas fa-trash"></i>
                            </a>
                          </td>
                      </tr>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/www-root/data/www/maqamsaz.kz/resources/views/admin/subscription.blade.php ENDPATH**/ ?>